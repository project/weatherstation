Weatherstation
==========

Module add block where background and text depend on weather.
We can also change background element we choose in config.

Images license https://www.pexels.com/photo-license/

Author/Maintainers
======================
- Pawel Gorski https://www.drupal.org/u/pawelgorski87
